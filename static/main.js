PROJECT_ID = 51693077;
GITLAB_URL="https://gitlab.com"
BASE_API = GITLAB_URL + "/api/v4/projects/" + PROJECT_ID + "/issues";
SHIELD_API="https://img.shields.io/badge/";

async function fetchBadgeData(){
    try{
        const response = await fetch(BASE_API);
        const data = await response.json();

        for (i in data) {

            color = "yellow";

            if(data[i].state == "opened"){
                color = "green";
            }
            if(data[i].state == "closed") {
                color = "blue";
            }
            title = data[i].title;
            title = title.replaceAll("-", "--");
            title = title.replaceAll("\"", "'");
            title = title.replaceAll("?", "%20");
            title = title.replaceAll(" ", "%20");
            img = SHIELD_API + title + "-" + data[i].state + "-" + color;

            var elem = document.createElement("img");
            elem.src = img;
            document.getElementById("badges").appendChild(elem);
            document.getElementById("badges").appendChild(document.createElement("br"));
        }
    } catch(err) {
        console.log(err);
    }
}
fetchBadgeData();
